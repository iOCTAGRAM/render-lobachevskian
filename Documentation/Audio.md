# Соединение двух песен:
Для их соедения необходимо условме sample rate должны быть одинаковые у файлов.

Пример изменения sample rate:

````
sox -r 16k 10917.wav 1.wav
````

Пример объединения двух файлов

````
sox -m 1.wav 2.wav midex.wav
````

## Сборка песни и видео:
ffmpeg -i video.mp4 -i audio.wav -filter_complex " [1:0] apad " -shortest -strict -2  result.mp4
