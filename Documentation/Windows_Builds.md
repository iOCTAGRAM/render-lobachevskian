# Сборка собственно исполнямых файлов:

Под Windows установка не отполирована, но у одного из разработчиков
Windows, так что всё возможно.

Текущая схема такая: можно взять любую достаточно продвинутую систему
сборки, загнать в неё Sources, Include, и ещё какой-то файл с main().
На данный момент файлы с main() находится в Tests и Tools.

Для них написаны GPR-проекты. Такие проекты собираются достаточно
продвинутой системой сборки gprbuild, которую можно поставить через
apt-get или скачать:

* https://www.adacore.com/download/more

````
D:\home\OCTAGRAM\render-lobachevskian\Tests\VR>gprbuild -Ptest_vr.gpr && test_vr.exe 
Compile
   [C++]          test_vr.cpp
Link
   [archive]      libtest_vr.a
   [index]        libtest_vr.a
   [link]         test_vr.cpp

Looking from origin
(-0.0325,0,0)---(0,0,-1)-->
(0.0325,0,0)---(0,0,-1)-->

Looking to the right a little
(-0.0325,0,-3.25e-005)---(0.001,0,-1)-->
(0.0325,0,3.25e-005)---(0.001,0,-1)-->

Looking up a little
(-0.0325,0,0)---(0,0.001,-1)-->
(0.0325,0,0)---(0,0.001,-1)-->

Looking forward from tilted camera
(-0.0281458,0.01625,0)---(0,0,-1)-->
(0.0281458,-0.01625,0)---(0,0,-1)-->

Flying around sphere, 0 degrees position
(-1.0325,-0,-0)---(0,0.00999983,-0.99995)-->
Flying around sphere, 60 degrees position
(-0.516249,-0,-0.89417)---(0.865982,0.00999983,-0.499975)-->
Flying around sphere, 120 degrees position
(0.516249,-0,-0.89417)---(0.865982,0.00999983,0.499975)-->
Flying around sphere, 180 degrees position
(1.0325,-0,-1.2644e-016)---(1.22455e-016,0.00999983,0.99995)-->
Flying around sphere, 240 degrees position
(0.516249,-0,0.89417)---(-0.865982,0.00999983,0.499975)-->
Flying around sphere, 300 degrees position
(-0.516249,-0,0.89417)---(-0.865982,0.00999983,-0.499975)-->
````
