#ifndef RENDER_LOBACHEVSKIAN_CAMERA3D
#define RENDER_LOBACHEVSKIAN_CAMERA3D

#include "geometry.h"
#include "vectors3d.h"
#include "quaternions.h"
#include "rays3d.h"

const coordinate ipd = 0.065;

// Our camera coordinate system is a right-handed 3D Cartesian system
// where positive x points to the right, positve y points up, and
// positive z points backward
class camera3d
{
    public:
    vector3d origin;
    coordinate zoom; // default IPD 0.065 m
    quaternion orientation;
    camera3d(const vector3d& origin_argument,
             const coordinate& zoom_argument,
             const quaternion& orientation_argument);
    ray3d emit_ray(const angle& theta, const angle& phi, bool is_right_eye) const;
};

#endif
