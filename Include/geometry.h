#ifndef RENDER_LOBACHEVSKIAN_GEOMETRY
#define RENDER_LOBACHEVSKIAN_GEOMETRY

#include <cmath>

// cmath for sqrt()

typedef double coordinate; // а может быть длинная арифметика с RAII
typedef double angle;

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#endif
