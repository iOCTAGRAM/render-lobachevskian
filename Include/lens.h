#ifndef RENDER_LOBACHEVSKIAN_LENS
#define RENDER_LOBACHEVSKIAN_LENS

#include "geometry.h"
#include "vectors3d.h"
#include "rays3d.h"
#include <vector>

const coordinate EPS = 1e-6;


coordinate dist_ray_from_sphere(const ray3d& light, const vector3d& shere);
ray3d ray_after_lens(const ray3d& light, const vector3d& shere, const coordinate r, const double k);
std::vector<vector3d> ray_separates_circle(const ray3d& light, const vector3d& shere, const coordinate r);

#endif