#ifndef RENDER_LOBACHEVSKIAN_QUATERNIONS
#define RENDER_LOBACHEVSKIAN_QUATERNIONS

#include "geometry.h"
#include "vectors3d.h"

// Our camera coordinate system is a right-handed 3D Cartesian system
// where positive x points to the right, positve y points up, and
// positive z points backward
class quaternion
{
    public:
    coordinate real;
    vector3d imaginary;
    quaternion(); // 1.0, no rotation
    quaternion(const coordinate& real_argument, const coordinate& x_argument, const coordinate& y_argument, const coordinate& z_argument);
    quaternion(const coordinate& real_argument, const vector3d& imaginary_argument);
    quaternion(const vector3d& axis, const angle& angle_argument);
    static quaternion multiply(const quaternion& left, const quaternion& right);
    static quaternion multiply(const quaternion& left, const coordinate& right);
    static quaternion multiply(const coordinate& left, const quaternion& right);
    static quaternion complement(const quaternion& item);
    static vector3d apply(const quaternion& left, const vector3d& right);
    static quaternion apply(const quaternion& left, const quaternion& right);
    static coordinate absolute(const quaternion& item);
    static quaternion make_unit(const quaternion& item);
    vector3d axis_x() const;
    vector3d axis_y() const;
    vector3d axis_z() const;
    vector3d axis_forward() const; // -z

};

#endif
