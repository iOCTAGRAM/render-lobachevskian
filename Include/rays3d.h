#ifndef RENDER_LOBACHEVSKIAN_RAYS3D
#define RENDER_LOBACHEVSKIAN_RAYS3D

#include "geometry.h"
#include "vectors3d.h"

// Our camera coordinate system is a right-handed 3D Cartesian system
// where positive x points to the right, positve y points up, and
// positive z points backward
class ray3d
{
    public:
    vector3d origin;
    vector3d direction;
    ray3d(const vector3d& origin, const vector3d& direction);
};


#endif
