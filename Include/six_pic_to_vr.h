#pragma once

#include <array>
#include <string>
#include <vector>
#include <iostream>
#include <functional>

#include "camera3d.h"


struct RGB {
    size_t r, g, b;

    RGB();
    RGB(size_t r, size_t g, size_t b);
    
    friend std::istream &operator >> (std::istream &is, RGB &rgb);
    friend std::ostream &operator << (std::ostream &os, const RGB &rgb);
};


class Photo {
    size_t n, m;
    std::string type;
    size_t max_color;
    std::vector<std::vector<RGB>> pixel;

 public:
    Photo();
    Photo(size_t n, size_t m, const std::string &type="P3", size_t max_color=255);

    size_t get_n() const;
    size_t get_m() const;

    void update(size_t row, size_t column, const RGB &rgb);
    RGB get(size_t row, size_t line) const;


    friend std::istream & operator >> (std::istream &is, Photo &photo);
    friend std::ostream & operator << (std::ostream &os, const Photo &photo);

};


// Photo six_pic_to_vr(const std::function<RGB(const ray3d& sight)>& ray_trace, size_t dimension, const camera3d& camera);
Photo six_pic_to_vr(const std::function<RGB(const ray3d& sight)>& ray_trace, size_t dimension, const camera3d& camera);

RGB choose_moving(const std::array<Photo, 6> &six_pic, const vector3d& direction);
coordinate maxarg(const vector3d &vec);
bool Eq(const coordinate &first, const coordinate &second);
RGB update_vr_true_pic(const Photo &pic, const coordinate &x, const coordinate & y);


