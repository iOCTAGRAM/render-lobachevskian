#ifndef RENDER_LOBACHEVSKIAN_VECTORS3D
#define RENDER_LOBACHEVSKIAN_VECTORS3D

#include "geometry.h"

// Our camera coordinate system is a right-handed 3D Cartesian system
// where positive x points to the right, positve y points up, and
// positive z points backward
class vector3d
{
    public:
    coordinate x;
    coordinate y;
    coordinate z;

    vector3d(const coordinate& x_argument, const coordinate& y_argument, const coordinate& z_argument);
    static coordinate scalar_multiply(const vector3d& left, const vector3d& right);
    static vector3d multiply(const vector3d& left, const coordinate& right);
    static vector3d multiply(const coordinate& left, const vector3d& right);
    static vector3d vector_multiply(const vector3d& left, const vector3d& right);
    static vector3d add(const vector3d& left, const vector3d& right);
    static vector3d subtract(const vector3d& left, const vector3d& right);
    static vector3d negate(const vector3d& item);
    static coordinate absolute(const vector3d& item);
    static vector3d make_unit(const vector3d& item);
};

#endif
