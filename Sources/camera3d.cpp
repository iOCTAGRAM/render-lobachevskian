#include "camera3d.h"

camera3d::camera3d(const vector3d& origin_argument,
                   const coordinate& zoom_argument,
                   const quaternion& orientation_argument)
    : origin(origin_argument),
      zoom(zoom_argument),
      orientation(orientation_argument)
{
}

// Our camera coordinate system is a right-handed 3D Cartesian system
// where positive x points to the right, positve y points up, and
// positive z points backward
ray3d camera3d::emit_ray(const angle& theta, const angle& phi, bool is_right_eye) const
{
    vector3d axis_x = orientation.axis_x();
    vector3d axis_y = orientation.axis_y();
    vector3d axis_z = orientation.axis_z();
    // rotate to theta
    vector3d forward_axis = vector3d::add(vector3d::multiply(axis_z, -cos(theta)),
                                          vector3d::multiply(axis_x, sin(theta)));
    vector3d right_axis = vector3d::add(vector3d::multiply(axis_x, cos(theta)),
                                        vector3d::multiply(axis_z, sin(theta)));
    const coordinate pole_merge = cos(phi); // can be more complex here
    const coordinate select_eye = is_right_eye ? 0.5 : -0.5;
    return ray3d(vector3d::add(origin, vector3d::multiply(ipd * pole_merge * select_eye / zoom, right_axis)),
                 vector3d::add(vector3d::multiply(sin(phi), axis_y),
                               vector3d::multiply(cos(phi), forward_axis)));
}
