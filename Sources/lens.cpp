#include "rays3d.h"
#include "vectors3d.h"
#include <vector>
#include "geometry.h"
#include <cmath>
#include "lens.h"

using namespace std;

coordinate dist_ray_from_sphere(const ray3d& light, const vector3d& sphere) {
	return vector3d::absolute(vector3d::vector_multiply(light.direction, vector3d::subtract(sphere, light.origin))) /
						 vector3d::absolute(light.direction);
}

ray3d ray_after_lens(const ray3d& light, const vector3d& sphere, const coordinate r, double k) {
	vector<vector3d>  seps = ray_separates_circle(light, sphere, r);
	if (seps.empty() || seps.size() == 1)
		return light;

	vector3d x = seps[0];
	vector3d norm1 = vector3d::multiply(vector3d::scalar_multiply(vector3d::subtract(sphere, x), light.direction) /
									 vector3d::absolute(vector3d::subtract(sphere, x)), vector3d::subtract(sphere, x));
	vector3d norm2 = vector3d::subtract(light.direction, norm1);
	
	vector3d xdir = vector3d::add(norm1, vector3d::multiply(k, norm2)); 
	ray3d xray = ray3d(x, xdir);
	
	ray3d temp = ray3d(vector3d::subtract(x, vector3d::multiply(100, xdir)), xdir);
	seps = ray_separates_circle(temp, sphere, r);
	vector3d y = seps[1];
	
	norm1 = vector3d::multiply(vector3d::scalar_multiply(vector3d::subtract(y, sphere), xdir) /
									 vector3d::absolute(vector3d::subtract(y, sphere)), vector3d::subtract(y, sphere));
	norm2 = vector3d::subtract(xdir, norm1);
	vector3d ydir = vector3d::add(norm1, vector3d::multiply(1 / k, norm2));
	ray3d yray = ray3d(y, ydir);
	
	return yray;
}

vector<vector3d> ray_separates_circle(const ray3d &light, const vector3d &sphere, const coordinate r) {
    coordinate dist = dist_ray_from_sphere(light, sphere);
    vector<vector3d> res;
    if (vector3d::scalar_multiply(light.direction, vector3d::subtract(sphere, light.origin)) <= 0)
    	return res;
    if (dist >= r + EPS){
    	return res;
    }
    if(abs(dist - r) < EPS){
        vector3d dlos = vector3d::subtract(sphere, light.origin);
        res.push_back(vector3d::add(light.origin, vector3d::multiply(light.direction, sqrt(vector3d::absolute(dlos) *
         			vector3d::absolute(dlos) - r * r) / vector3d::absolute(light.direction))));
        return res;
    }
    else{
        vector3d dlos = vector3d::subtract(sphere, light.origin);
        coordinate dres = sqrt(vector3d::absolute(dlos) * vector3d::absolute(dlos) - dist * dist) - sqrt(r * r - dist * dist);
        res.push_back(vector3d::add(light.origin,vector3d::multiply(light.direction, dres / vector3d::absolute(light.direction))));
        dres = sqrt(vector3d::absolute(dlos) * vector3d::absolute(dlos) - dist * dist) + sqrt(r * r - dist * dist);
        res.push_back(vector3d::add(light.origin,vector3d::multiply(light.direction, dres / vector3d::absolute(light.direction))));
        return res;
    }
}