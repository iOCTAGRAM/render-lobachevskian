#include "quaternions.h"

quaternion::quaternion()
    : real(1.0), imaginary(0.0, 0.0, 0.0)
{
}

quaternion::quaternion
  (const coordinate& real_argument,
   const coordinate& x_argument,
   const coordinate& y_argument,
   const coordinate& z_argument)
    : real(real_argument), imaginary(x_argument, y_argument, z_argument)
{
}

quaternion::quaternion
  (const coordinate& real_argument,
   const vector3d& imaginary_argument)
    : real(real_argument), imaginary(imaginary_argument)
{
}

quaternion::quaternion(const vector3d& axis, const angle& angle_argument)
    : real(cos(angle_argument / 2)),
      imaginary(vector3d::multiply(vector3d::make_unit(axis), sin(angle_argument / 2)))
{
}

quaternion quaternion::multiply(const quaternion& left, const quaternion& right)
{
    return quaternion
        (left.real * right.real - vector3d::scalar_multiply(left.imaginary, right.imaginary),
         vector3d::add(
             vector3d::add(vector3d::multiply(left.real, right.imaginary),
                           vector3d::multiply(left.imaginary, right.real)),
             vector3d::vector_multiply(left.imaginary, right.imaginary)));
}

quaternion quaternion::multiply(const quaternion& left, const coordinate& right)
{
    return quaternion(left.real * right, vector3d::multiply(left.imaginary, right));
}

quaternion quaternion::multiply(const coordinate& left, const quaternion& right)
{
    return quaternion(left * right.real, vector3d::multiply(left, right.imaginary));
}

quaternion quaternion::complement(const quaternion& item)
{
    return quaternion(item.real, vector3d::negate(item.imaginary));
}

vector3d quaternion::apply(const quaternion& left, const vector3d& right)
{
    return quaternion::multiply(quaternion::multiply(left, quaternion(0.0, right)),
                                quaternion::complement(left)).imaginary;
}

quaternion quaternion::apply(const quaternion& left, const quaternion& right)
{
    return quaternion::multiply(quaternion::multiply(left, right),
                                quaternion::complement(left));
}

coordinate quaternion::absolute(const quaternion& item)
{
    return sqrt(item.real * item.real +
                item.imaginary.x * item.imaginary.x +
                item.imaginary.y * item.imaginary.y +
                item.imaginary.z * item.imaginary.z);
}

quaternion quaternion::make_unit(const quaternion& item)
{
    return quaternion::multiply(item, 1.0 / quaternion::absolute(item));
}

vector3d quaternion::axis_x() const
{
    return quaternion::apply(*this, vector3d(1.0, 0.0, 0.0));
}

vector3d quaternion::axis_y() const
{
    return quaternion::apply(*this, vector3d(0.0, 1.0, 0.0));
}

vector3d quaternion::axis_z() const
{
    return quaternion::apply(*this, vector3d(0.0, 0.0, 1.0));
}

vector3d quaternion::axis_forward() const
{
    return quaternion::apply(*this, vector3d(0.0, 0.0, -1.0));
}
