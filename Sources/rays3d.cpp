#include "rays3d.h"

ray3d::ray3d(const vector3d& origin_argument,
             const vector3d& direction_argument)
  : origin(origin_argument), direction(direction_argument)
{
}
