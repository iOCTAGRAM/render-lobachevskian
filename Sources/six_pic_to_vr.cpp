#include <cmath>
#include <fstream>
#include <assert.h>


#include "six_pic_to_vr.h"

using namespace std;


istream &operator >>(istream &is, RGB &rgb) {
    return is >> rgb.r >> rgb.g >> rgb.b;
}

ostream &operator <<(ostream &os, const RGB &rgb) {
    return os << rgb.r << ' ' << rgb.g << ' ' << rgb.b;
}


istream & operator >> (istream &is, Photo &photo) {
    getline(is, photo.type);
    is >> photo.n >> photo.m;
    is >> photo.max_color;
    photo.pixel = vector<vector<RGB>> (photo.n, vector<RGB> (photo.m));
    for (auto& row : photo.pixel) {
        for (auto &pixel : row) {
            is >> pixel;
        }
    }
    return is;
}


ostream & operator << (ostream &os, const Photo &photo) {
    os << photo.type << endl;
    os << photo.n << ' ' << photo.m << endl;
    os << photo.max_color << endl;
    size_t size_line = 6;
    for (auto& row : photo.pixel) {
        size_t counter = 0;
        for (auto &pixel : row) {
            os << pixel << "  ";
            ++counter;
            if (counter == 6) {
                counter = 0;
                os << endl;
            }
        }
        os << endl;
    }
    return os;
}


Photo::Photo(size_t n, size_t m, const string& type, size_t max_color):
    n(n), 
    m(m),
    type(type),
    max_color(max_color), 
    pixel(vector<vector<RGB>> (n, vector<RGB>(m)))
{}


Photo::Photo()
{}


size_t Photo::get_n() const {
    return n;
}
    

size_t Photo::get_m() const {
    return m;
}

/*
Photo six_pic_to_vr(const std::function<RGB(const ray3d& sight)>& ray_trace, size_t dimension, const camera3d& camera) {
    array<Photo, 6> picture; 
    for (size_t index = 0; index < picture.size(); ++index) {
        ifstream is(path_to_pic[index]);
        is >> picture[index];
    }
    return six_pic_to_vr(picture, dimension, camera);
}
*/

Photo six_pic_to_vr(const std::function<RGB(const ray3d& sight)>& ray_trace, size_t dimension, const camera3d& camera) {
    Photo res(dimension, dimension);
    
    for (size_t row_index = 0; row_index < res.get_n() / 2; ++row_index) {
        for (size_t column_index = 0; column_index < res.get_m(); ++column_index) {
            double x = (column_index + 0.5) / dimension; 
            double y = (row_index + 0.5) * 2.0 / dimension;
            double theta = x * 2 * M_PI - M_PI;
            double phi = M_PI / 2 - y * M_PI;
            // this is left eye
            size_t left_eye = row_index;
            res.update(left_eye, column_index, ray_trace(camera.emit_ray(theta, phi, false)));
            // this is right eye
            size_t right_eye = row_index + res.get_n() / 2;
            res.update(right_eye, column_index, ray_trace(camera.emit_ray(theta, phi, true)));
            //res.update(right_eye, column_index, RGB(0, 0, 0)); for delete right eye
        }
    }
    return res;
}


RGB::RGB()
{}


RGB::RGB(size_t r, size_t g, size_t b):
    r(r), g(g), b(b)
{}


void Photo::update(size_t row, size_t column, const RGB &rgb) {
    pixel[row][column] = rgb;
}


RGB choose_moving(const std::array<Photo, 6> &six_pic, const vector3d& direction) {
    coordinate maxarg_for_direction = maxarg(direction);
    const coordinate &x = direction.x;
    const coordinate &y = direction.y;
    const coordinate &z = direction.z;
    if (Eq(maxarg_for_direction, abs(x))) {
        if (direction.x > 0) {
            return update_vr_true_pic(six_pic[2], z / abs(x), -y / abs(x));
        } else {
            return update_vr_true_pic(six_pic[3], -z / abs(x), -y / abs(x));
        }
    } else if (Eq(maxarg_for_direction, abs(y))) {
        if (direction.y > 0) {
            return update_vr_true_pic(six_pic[4], x / abs(y), -z / abs(y));
        } else {
            return update_vr_true_pic(six_pic[5], x / abs(y), z / abs(y));
        }
    } else if (Eq(maxarg_for_direction, abs(z))) {
        if (direction.z > 0) {
            return update_vr_true_pic(six_pic[1], -x / abs(z), -y / abs(z));
        } else {
            return update_vr_true_pic(six_pic[0], x / abs(z), -y / abs(z));
        } 
    } else {
        cerr << "Warning: have direction without maxargs" << endl;
        cerr << direction.x << ' ' << direction.y << ' ' << direction.z << endl;
        return RGB(0, 0, 0);
    }
}


RGB update_vr_true_pic(const Photo &pic, const coordinate& x, const coordinate& y) {
    size_t row = static_cast<size_t>((pic.get_n()) * ((y + 1) / 2.));
    size_t column = static_cast<size_t>((pic.get_m()) * ((x + 1) / 2.));
    if (row >= pic.get_n())
    {
        row = pic.get_n() - 1;
    }
    if (column >= pic.get_m())
    {
        column = pic.get_m() - 1;
    }
    return pic.get(row, column);
}

RGB Photo::get(size_t row, size_t column) const {
    assert(row < n);
    assert(column < m);
    return pixel[row][column];
}


coordinate maxarg(const vector3d &vec) {
    return max(max(abs(vec.x), abs(vec.y)), abs(vec.z));
}


bool Eq(const coordinate &first, const coordinate &second) {
    return abs(first - second) < 1e-6;
}
