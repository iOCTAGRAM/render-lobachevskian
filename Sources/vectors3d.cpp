#include "geometry.h"
#include "rays3d.h"
#include "vectors3d.h"

vector3d::vector3d(const coordinate& x_argument, const coordinate& y_argument, const coordinate& z_argument)
  : x(x_argument), y(y_argument), z(z_argument)
{
    x = x_argument;
    y = y_argument;
    z = z_argument;
}

coordinate vector3d::scalar_multiply(const vector3d& left, const vector3d& right)
{
    return left.x * right.x + left.y * right.y + left.z * right.z;
}

vector3d vector3d::multiply(const vector3d& left, const coordinate& right)
{
    return vector3d(left.x * right, left.y * right, left.z * right);
}

vector3d vector3d::multiply(const coordinate& left, const vector3d& right)
{
    return vector3d(left * right.x, left * right.y, left * right.z);
}

vector3d vector3d::vector_multiply(const vector3d& left, const vector3d& right)
{
    return vector3d(left.y * right.z - left.z * right.y,  left.z * right.x - left.x * right.z, left.x * right.y - left.y * right.x);
}

vector3d vector3d::add(const vector3d& left, const vector3d& right)
{
    return vector3d(left.x + right.x, left.y + right.y, left.z + right.z);
}

vector3d vector3d::subtract(const vector3d& left, const vector3d& right)
{
    return vector3d(left.x - right.x, left.y - right.y, left.z - right.z);
}

vector3d vector3d::negate(const vector3d& item)
{
    return vector3d(-item.x, -item.y, -item.z);
}

coordinate vector3d::absolute(const vector3d& item)
{
    return sqrt(item.x * item.x + item.y * item.y + item.z * item.z);
}

vector3d vector3d::make_unit(const vector3d& item)
{
    return vector3d::multiply(item, 1.0 / vector3d::absolute(item));
}