#include <iostream>
#include <string>
#include <array>
#include <fstream>
#include <cmath>

#include "six_pic_to_vr.h"


using namespace std;


void test(const string& out, const array<string, 6> &pictures) {
    ofstream os(out);
    camera3d camera(vector3d(0.0, 0.0, 0.0),
                    1.0,
                    quaternion());
    os << six_pic_to_vr(pictures, 512, camera);
    cout << "closed without backs" << endl;
    cout << "result in file: " << out << endl;
}


int main() {
    array<string, 6> first_pictures = {
        "data1/SkyboxTex1.pnm",
        "data1/SkyboxTex2.pnm",
        "data1/SkyboxTex3.pnm",
        "data1/SkyboxTex4.pnm",
        "data1/SkyboxTex5.pnm",
        "data1/SkyboxTex6.pnm"
    };
    test("result_date1.pnm", first_pictures);

    array<string, 6> second_pictures = {
        "data2/test1.pnm",
        "data2/test2.pnm",
        "data2/test3.pnm",
        "data2/test4.pnm",
        "data2/test5.pnm",
        "data2/test6.pnm"
    };
    test("result_date2.pnm", second_pictures);
    return 0; 
}  
