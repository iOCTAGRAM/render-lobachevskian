#include <iostream>
#include "camera3d.h"

void print_vector(const vector3d& item)
{
    std::cout << "(" << item.x << "," << item.y << "," << item.z << ")";
}

void print_ray(const ray3d& item)
{
    print_vector(item.origin);
    std::cout << "---";
    print_vector(item.direction);
    std::cout << "-->";
}

int main()
{
    camera3d test_camera(vector3d(0.0, 0.0, 0.0),
                         1.0,
                         quaternion());
    std::cout << "Looking from origin" << std::endl;
    print_ray(test_camera.emit_ray(0.0, 0.0, false)); std::cout << std::endl;
    print_ray(test_camera.emit_ray(0.0, 0.0, true)); std::cout << std::endl;
    // (-0.0325,0,0)---(0,0,-1)-->
    // (0.0325,0,0)---(0,0,-1)-->
    std::cout << std::endl;
    
    std::cout << "Looking to the right a little" << std::endl;
    print_ray(test_camera.emit_ray(0.001, 0.0, false)); std::cout << std::endl;
    print_ray(test_camera.emit_ray(0.001, 0.0, true)); std::cout << std::endl;
    // (-0.0325,0,-3.25e-005)---(0.001,0,-1)-->
    // (0.0325,0,3.25e-005)---(0.001,0,-1)-->
    std::cout << std::endl;

    std::cout << "Looking up a little" << std::endl;
    print_ray(test_camera.emit_ray(0.0, 0.001, false)); std::cout << std::endl;
    print_ray(test_camera.emit_ray(0.0, 0.001, true)); std::cout << std::endl;
    // (-0.0325,0,0)---(0,0.001,-1)-->
    // (0.0325,0,0)---(0,0.001,-1)-->
    std::cout << std::endl;

    std::cout << "Looking forward from tilted camera" << std::endl;
    test_camera.orientation = quaternion(vector3d(0.0, 0.0, -1.0), M_PI / 6);
    print_ray(test_camera.emit_ray(0.0, 0.0, false)); std::cout << std::endl;
    print_ray(test_camera.emit_ray(0.0, 0.0, true)); std::cout << std::endl;
    // (-0.0281458,0.01625,0)---(0,0,-1)-->
    // (0.0281458,-0.01625,0)---(0,0,-1)-->
    std::cout << std::endl;

    for (int angle_divide_60 = 0; angle_divide_60 < 6; angle_divide_60 += 1)
    {
        std::cout << "Flying around sphere, " << (angle_divide_60 * 60) << " degrees position" << std::endl;
        test_camera.orientation = quaternion(vector3d(0.0, 1.0, 0.0), -M_PI * (angle_divide_60 * 60.0) / 180);
        test_camera.origin = vector3d::negate(test_camera.orientation.axis_x());
        print_ray(test_camera.emit_ray(0.0, 0.01, false)); std::cout << std::endl;
    }
    std::cout << std::endl;

    std::cout << std::endl;
    return 0;
}
