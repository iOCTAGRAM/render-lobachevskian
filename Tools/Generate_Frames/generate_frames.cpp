#include <iostream>
#include <string>
#include <array>
#include <fstream>
#include <cmath>
#include "lens.h"

#include "six_pic_to_vr.h"


using namespace std;


//const size_t dimension = 512;
//const int frame_count = 4;
const coordinate anomaly_radius = 8.0;
const vector3d anomaly_center(0.0, 0.0, 0.0);

RGB ray_trace_backgroung(const array<Photo, 6>& pictures, const ray3d& sight)
{
    return choose_moving(pictures, sight.direction);
}

void generate_frame(const string& out, const array<Photo, 6>& pictures, int frame, const int dimension)
{
    ofstream os(out);
    quaternion orientation(vector3d(0.0, 1.0, 0.0), -frame * M_PI / 180);
    camera3d camera(vector3d::multiply(orientation.axis_x(), -10.6),
                    1.0,
                    orientation);

    os << six_pic_to_vr([&] (const ray3d& sight) -> RGB
    {
        vector3d from_anomaly_center = vector3d::subtract(sight.origin, anomaly_center);
        coordinate from_center_mul_direction = vector3d::scalar_multiply(from_anomaly_center, sight.direction);
        if (from_center_mul_direction < 0)
        {
            // we look at sphere
            vector3d opposite_to_sight_component =
                vector3d::multiply(from_center_mul_direction, sight.direction);
            vector3d normal_to_sight_component =
                vector3d::subtract(from_anomaly_center, opposite_to_sight_component);
            coordinate normal_to_sight_component_absolute = vector3d::absolute(normal_to_sight_component);
            if (normal_to_sight_component_absolute < anomaly_radius)
            {
            	//return RGB(185, 185, 185);
                return ray_trace_backgroung(pictures, ray_after_lens(sight, anomaly_center, anomaly_radius, 1.09));
            }
        }
        return ray_trace_backgroung(pictures, sight);
    }, dimension, camera);
}

int main(int argc, char** argv) {
    int dimension = atoi(argv[1]);
    int frame_count = atoi(argv[2]);
    cout << dimension << ' ' << frame_count << endl;
    array<Photo, 6> skybox_pictures;

    {
        cout << "Reading textures...";
        array<string, 6> path_to_pic = {
            "Media/Skins/SkyboxTex1.pnm",
            "Media/Skins/SkyboxTex2.pnm",
            "Media/Skins/SkyboxTex3.pnm",
            "Media/Skins/SkyboxTex4.pnm",
            "Media/Skins/SkyboxTex5.pnm",
            "Media/Skins/SkyboxTex6.pnm"
        };

        for (size_t index = 0; index < path_to_pic.size(); ++index) {
            ifstream is(path_to_pic[index]);
            is >> skybox_pictures[index];
            cout << " " << (index + 1);
        }
        cout << " done!" << endl;
    }

    for (int frame = 0; frame < frame_count; ++frame)
    {
        cout << "Generating frame " << (frame + 1) << " of " << frame_count << "...";
        generate_frame(std::string("Media/Output_PNM/frame") + std::to_string(frame) + ".pnm", skybox_pictures, frame, dimension);
        cout << " done!" << endl;
    }
    return 0; 
}
