#include <iostream>
#include <string>
#include <array>
#include <fstream>
#include <cmath>
#include <iomanip>
#include "lens.h"

#include "six_pic_to_vr.h"


using namespace std;


const coordinate anomaly_radius = 8.0;
const vector3d anomaly_center(0.0, 0.0, 0.0);

RGB ray_trace_backgroung(const array<Photo, 6>& pictures, const ray3d& sight)
{
    return choose_moving(pictures, sight.direction);
}

void generate_frame(const string& out, const array<Photo, 6>& pictures, 
    const camera3d &camera, int frame, const int dimension)
{
    ofstream os(out);
    os << six_pic_to_vr([pictures] (const ray3d& sight) -> RGB
    {
        vector3d from_anomaly_center = vector3d::subtract(sight.origin, anomaly_center);
        coordinate from_center_mul_direction = vector3d::scalar_multiply(from_anomaly_center, sight.direction);
        if (from_center_mul_direction < 0)
        {
            // we look at sphere
            vector3d opposite_to_sight_component =
                vector3d::multiply(from_center_mul_direction, sight.direction);
            vector3d normal_to_sight_component =
                vector3d::subtract(from_anomaly_center, opposite_to_sight_component);
            coordinate normal_to_sight_component_absolute = vector3d::absolute(normal_to_sight_component);
            if (normal_to_sight_component_absolute < anomaly_radius)
            {
            	//return RGB(185, 185, 185);
                return ray_trace_backgroung(pictures, ray_after_lens(sight, anomaly_center, anomaly_radius, 1.09));
            }
        }
        return ray_trace_backgroung(pictures, sight);
    }, dimension, camera);
}


void print(const quaternion &v) {
    cerr << v.real << ' ' << v.imaginary.x << ' '<< v.imaginary.y << ' ' << v.imaginary.z << endl; 
}

void print(const vector3d &v) {
    cerr << v.x << ' '<< v.y << ' ' << v.z << endl; 
}

void change_camera_by_alph_with_coef(camera3d &camera, double alph, double coef) {
    camera.origin = vector3d::multiply(coef, quaternion::apply(quaternion(vector3d(0, 1, 0), alph), camera.origin));
    quaternion &quat = camera.orientation;
    quat = quaternion::multiply(quat, quaternion(vector3d(0, 1, 0), alph));
}


int main(int argc, char** argv) {
    srand(4);
    int dimension = atoi(argv[1]);
    int frame_count = atoi(argv[2]);
    array<Photo, 6> skybox_pictures;
    {
        cout << "Reading textures...";
        array<string, 6> path_to_pic = {
            "Media/Skins/SkyboxTex1.pnm",
            "Media/Skins/SkyboxTex2.pnm",
            "Media/Skins/SkyboxTex3.pnm",
            "Media/Skins/SkyboxTex4.pnm",
            "Media/Skins/SkyboxTex5.pnm",
            "Media/Skins/SkyboxTex6.pnm"
        };
        for (size_t index = 0; index < path_to_pic.size(); ++index) {
            ifstream is(path_to_pic[index]);
            is >> skybox_pictures[index];
            cout << " " << (index + 1);
        }
        cout << " done!" << endl;
    }

    quaternion orientation(vector3d(0.0, 1.0, 0.0), 0);
    camera3d camera(vector3d::multiply(orientation.axis_x(), -15.6),
                    1.0,
                    orientation);
    double coef_extension = 0.9;
    double alph = 10 * M_PI / 180;
    for (int frame = 0; frame < frame_count; ++frame)
    {
        if (frame % 20 == 0) {
            // this is hard code. based on visual effects
            coef_extension = 2 - coef_extension;
        }
        cout << "Generating frame " << (frame + 1) << " of " << frame_count << "...";
        generate_frame(std::string("Media/Output_PNM/") + std::to_string(frame) + ".pnm", 
            skybox_pictures, camera, frame, dimension);
        change_camera_by_alph_with_coef(camera, alph, coef_extension);
        cout << " done!" << endl;
    }
    return 0; 
}
