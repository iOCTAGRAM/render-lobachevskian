#!/usr/bin/env bash

if [ "$#" -ne 4 ]; then
    echo "Usage: produce.sh -d 720 -n 200" >&2
    exit 1
fi

while getopts "d:n:" opt; do
  case $opt in
    d) dim="$OPTARG"
    ;;
    n) num_frames="$OPTARG"
    ;;
    :) echo "Option -$OPTARG requires an argument." >&2
    exit 1
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    exit 1    
    ;;
  esac
done

set -e

DIR=`dirname "$0"`
cd "$DIR"

echo -n "Checking for gprbuild ... "; which gprbuild || { echo "not found"; exit 1; }
echo -n "Checking for pnmtopng ... "; which pnmtopng || { echo "not found"; exit 1; }
echo -n "Checking for ffmpeg ... "; which ffmpeg || { echo "not found"; exit 1; }
echo -n "Checking for MP4Box ... "; which MP4Box || { echo "not found"; exit 1; }
echo -n "Checking for python ... "; which python || { echo "not found"; exit 1; }

pathtotba="Tools/TurnByAlphWithCoefExtension"
pathtogf="Tools/Generate_Frames"
pathtosm="Tools/spatial-media"
pathtopnm="Media/Output_PNM/"
pathtopng="Media/Output_PNG/"
pathtovideo="Media/Video/"
urltosm="https://github.com/google/spatial-media" 

#clear previous media
for i in $pathtovideo $pathtopng $pathtopnm
do
	rm -rf $i*
	touch $i.gitkeep
done

#build executable file

gprbuild -P $pathtogf/generate_frames.gpr
gprbuild -P $pathtotba/turn_by_alph_with_coef_extension.gpr
#create frames .pnm

$pathtogf/generate_frames $dim $num_frames
$pathtotba/turn_by_alph_with_coef_extension $dim $num_frames

#convert .pnm to .png

for file in $pathtopnm*
do
	if [ -f "$file" ]
	then
		filename=$(basename -- "$file")
		filename="${filename%.*}"
		pnmtopng "$file" > $pathtopng/$filename.png
	fi
done

#create .mp4 w/o meta data

ffmpeg -framerate 30 -y -i $pathtopng/frame%d.png $pathtovideo/video.mp4
ffmpeg -framerate 30 -y -i $pathtopng/%d.png $pathtovideo/video1.mp4


#concat two parts
MP4Box -add $pathtovideo/video.mp4 -cat $pathtovideo/video1.mp4 $pathtovideo/video2.mp4 

#create .mp4 with meta data

if [ ! -d "$pathtosm" ]; then
	(cd Tools/; git clone "$urltosm")
fi

python $pathtosm/spatialmedia -i --stereo=top-bottom $pathtovideo/video2.mp4 $pathtovideo/result.mp4
#enjoy via youtube.com


exit 0